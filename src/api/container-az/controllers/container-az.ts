/**
 * container-az controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::container-az.container-az');
