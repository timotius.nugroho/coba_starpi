/**
 * container-az service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::container-az.container-az');
