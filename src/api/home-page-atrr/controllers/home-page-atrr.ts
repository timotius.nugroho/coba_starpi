/**
 * home-page-atrr controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::home-page-atrr.home-page-atrr');
