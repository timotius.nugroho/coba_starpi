/**
 * home-page-atrr router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::home-page-atrr.home-page-atrr');
