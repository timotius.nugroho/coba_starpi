/**
 * home-page-atrr service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::home-page-atrr.home-page-atrr');
