/**
 * truck-container router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::truck-container.truck-container');
