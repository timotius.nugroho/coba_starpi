/**
 * truck-container service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::truck-container.truck-container');
