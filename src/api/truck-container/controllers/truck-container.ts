/**
 * truck-container controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::truck-container.truck-container');
