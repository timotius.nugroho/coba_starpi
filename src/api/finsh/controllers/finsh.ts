/**
 * finsh controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::finsh.finsh');
