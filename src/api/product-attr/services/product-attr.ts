/**
 * product-attr service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::product-attr.product-attr');
