/**
 * product-attr controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::product-attr.product-attr');
